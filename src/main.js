import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import installElementPlus from './plugins/element'

// 创建App.vue对象
const app = createApp(App)
// 安装elementUI
installElementPlus(app)
// 加载路由
app.use(router).mount('#app')
